package ru.art;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ContextFactory {
    private static ApplicationContext context;

    private ContextFactory() {
    }

    public static ApplicationContext getContext() {
        if (context == null) {
            synchronized (ContextFactory.class) {
                if (context == null) {
                    context = new AnnotationConfigApplicationContext(Main.class);
                }
            }
        }
        return context;
    }
}
