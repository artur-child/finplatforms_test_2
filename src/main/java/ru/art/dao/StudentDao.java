package ru.art.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.art.service.Converter;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentDao {
    private final ConnectionFactory connectionFactory;
    private final Converter converter;

    public StudentModel addStudent(StudentModel studentModel) {
        Connection connection = connectionFactory.getConnection();
        StudentModel returnModel = null;
        try {
            PreparedStatement ps = connection.prepareStatement("insert into students (first_name, second_name, third_name, birth_date, gruppa) " +
                            "values (?, ?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, studentModel.getFirstName());
            ps.setString(2, studentModel.getSecondName());
            ps.setString(3, studentModel.getThirdName());
            ps.setObject(4, studentModel.getBirthDate());
            ps.setString(5, studentModel.getGroup());
            ps.executeUpdate();

            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                returnModel = new StudentModel()
                        .setId(generatedKeys.getLong(1))
                        .setFirstName(studentModel.getFirstName())
                        .setSecondName(studentModel.getSecondName())
                        .setThirdName(studentModel.getThirdName())
                        .setBirthDate(studentModel.getBirthDate())
                        .setGroup(studentModel.getGroup());
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            connectionFactory.closeConnection(connection);
        }
        return returnModel;
    }

    public List<StudentModel> listOfStudents() {
        Connection connection = connectionFactory.getConnection();
        List<StudentModel> result = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement("select * from students");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                StudentModel studentModel = new StudentModel()
                        .setId(rs.getLong("id"))
                        .setFirstName(rs.getString("first_name"))
                        .setSecondName(rs.getString("second_name"))
                        .setThirdName(rs.getString("third_name"))
                        .setBirthDate(rs.getObject("birth_date", LocalDate.class))
                        .setGroup(rs.getString("gruppa"));
                result.add(studentModel);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            connectionFactory.closeConnection(connection);
        }
        return result;
    }


    public boolean deleteStudent(StudentModel studentModel) {
        Connection connection = connectionFactory.getConnection();
        Long id = studentModel.getId();
        try {
            PreparedStatement ps = connection.prepareStatement("delete from students where id = ?");
            ps.setLong(1, id);
            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            connectionFactory.closeConnection(connection);
        }
    }

}
