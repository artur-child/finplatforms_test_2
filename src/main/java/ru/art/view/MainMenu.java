package ru.art.view;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.art.service.StudentService;

import static ru.art.view.ViewUtil.getString;

@RequiredArgsConstructor
@Service
public class MainMenu {
    private final StudentService studentService;
    private final AddView addView;
    private final DelView delView;

    public void menu() {

        String inputText;
        do {
            inputText = getString("Main menu.\nEnter 'exit' to exit. \nEnter 'list' to get student list. " +
                    "\nEnter 'add' to add student. \nEnter 'del' to delete student:");
            if (inputText.equals("list")) {
                System.out.println(studentService.listOfStudents());
            }
            if (inputText.equals("add")) {
                addView.addAction();
            }
            if (inputText.equals("del")) {
                delView.delAction();
            }
        } while (!inputText.equals("exit"));
    }


}
