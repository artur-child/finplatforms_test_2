package ru.art.view;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.art.service.StudentDTO;
import ru.art.service.StudentService;

import static ru.art.view.ViewUtil.getString;

@Service
@RequiredArgsConstructor
public class DelView {
    private final StudentService studentService;

    public void delAction() {
        String studentId = getString("Enter student ID");
        Long id = Long.parseLong(studentId);
        StudentDTO studentDTO = new StudentDTO().setId(id);

        if (studentService.deleteStudent(studentDTO)) {
            System.out.println("Delete success");
        } else {
            System.out.println("Delete fail");
        }

    }
}
