package ru.art.view;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.art.service.StudentDTO;
import ru.art.service.StudentService;

import java.time.LocalDate;

import static ru.art.view.ViewUtil.*;

@RequiredArgsConstructor
@Service
public class AddView {
    private final StudentService studentService;

    public void addAction() {
        String firstName = getString("Enter first name");
        String secondName = getString("Enter second name");
        String thirdName = getString("Enter third name");
        String birthDateString = getString("Enter birth date (YYYY.MM.DD)");
        LocalDate birthDate;
        if (isStringValidLocalDate(birthDateString)) {
            birthDate = parseStringToLocalDate(birthDateString);
        } else {
            System.out.println("Wrong date format");
            return;
        }
        String group = getString("Enter group name");

        StudentDTO studentDTO = new StudentDTO()
                .setFirstName(firstName)
                .setSecondName(secondName)
                .setThirdName(thirdName)
                .setBirthDate(birthDate)
                .setGroup(group);
        if (studentService.addStudent(studentDTO)) {
            System.out.println("Add success");
        } else {
            System.out.println("Add fail");
        }
    }

}
