package ru.art.view;

import java.time.LocalDate;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class ViewUtil {
    static String getString(String text) {
        Scanner in = new Scanner(System.in);
        System.out.println(text);
        return in.next();
    }

    static boolean isStringValidLocalDate(String s) {
        return s.matches("\\d{4}[.]\\d{2}[.]\\d{2}");
    }

    static LocalDate parseStringToLocalDate(String s) {
        String[] split = s.split("\\.");
        return LocalDate.of(
                parseInt(split[0]),
                parseInt(split[1]),
                parseInt(split[2]));
    }
}
