package ru.art;

import org.springframework.context.annotation.ComponentScan;
import ru.art.view.MainMenu;

import static ru.art.ContextFactory.getContext;


@ComponentScan
public class Main {
    public static void main(String[] args) {
        MainMenu mainMenu = getContext().getBean(MainMenu.class);
        mainMenu.menu();
    }

}
