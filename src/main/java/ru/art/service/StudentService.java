package ru.art.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.art.dao.StudentDao;
import ru.art.dao.StudentModel;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class StudentService {
    private final Converter converter;
    private final StudentDao studentDao;

    public boolean addStudent(StudentDTO studentDTO) {
        StudentModel studentModel = converter.convert(studentDTO, StudentModel.class);
        return studentDao.addStudent(studentModel) != null;
    }

    public boolean deleteStudent(StudentDTO studentDTO) {
        StudentModel studentModel = converter.convert(studentDTO, StudentModel.class);
        return studentDao.deleteStudent(studentModel);
    }

    public List<StudentDTO> listOfStudents() {
        return studentDao.listOfStudents().stream()
                .map(m -> converter.convert(m, StudentDTO.class))
                .collect(Collectors.toList());
    }

}
