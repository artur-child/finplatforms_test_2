package ru.art.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static ru.art.ContextFactory.getContext;

class StudentDaoTest {
    StudentDao subj;

    @BeforeEach
    void setUp() {
        System.setProperty("jdbcUrl", "jdbc:h2:mem:test_mem" + UUID.randomUUID());
        System.setProperty("dataSource.user", "sa");
        System.setProperty("dataSource.password", "");
        System.setProperty("liquibaseFile", "dbchangelog_student_dao_test.xml");

        subj = getContext().getBean(StudentDao.class);
    }

    @Test
    void addStudent() {
        StudentModel expected = new StudentModel()
                .setFirstName("first_name_4")
                .setSecondName("second_name_4")
                .setThirdName("third_name_4")
                .setBirthDate(LocalDate.of(2000, 2, 4))
                .setGroup("gruppa_4");

        StudentModel actual = subj.addStudent(expected);
        assertEquals(expected.setId(4L), actual);
    }

    @Test
    void listOfStudents() {
        StudentModel studentModel1 = new StudentModel()
                .setId(1L)
                .setFirstName("first_name_1")
                .setSecondName("second_name_1")
                .setThirdName("third_name_1")
                .setBirthDate(LocalDate.of(2000, 2, 1))
                .setGroup("gruppa_1");
        StudentModel studentModel2 = new StudentModel()
                .setId(2L)
                .setFirstName("first_name_2")
                .setSecondName("second_name_2")
                .setThirdName("third_name_2")
                .setBirthDate(LocalDate.of(2000, 2, 2))
                .setGroup("gruppa_2");

        List<StudentModel> studentModels = subj.listOfStudents();
        assertEquals(studentModels.get(0), studentModel1);
        assertEquals(studentModels.get(1), studentModel2);
    }

    @Test
    void deleteStudent_success() {
        boolean b = subj.deleteStudent(new StudentModel().setId(3L));
        assertTrue(b);
    }

    @Test
    void deleteStudent_failDueToNoSuchStudentId() {
        boolean b = subj.deleteStudent(new StudentModel().setId(30L));
        assertFalse(b);
    }
}