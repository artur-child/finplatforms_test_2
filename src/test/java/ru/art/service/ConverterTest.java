package ru.art.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.art.dao.StudentModel;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class ConverterTest {
    @InjectMocks
    Converter converter;

    @Test
    void convert_fromStudentDTOToModel() {
        StudentDTO studentDTO = new StudentDTO()
                .setFirstName("first_name_4")
                .setSecondName("second_name_4")
                .setThirdName("third_name_4")
                .setBirthDate(LocalDate.of(2000, 2, 4))
                .setGroup("gruppa_4");

        StudentModel studentModel = converter.convert(studentDTO, StudentModel.class);
        StudentDTO convert = converter.convert(studentModel, StudentDTO.class);

        assertEquals(studentDTO, convert);
    }

    @Test
    void convert_fromStudentModelToDTO() {
        StudentModel studentModel = new StudentModel()
                .setFirstName("first_name_4")
                .setSecondName("second_name_4")
                .setThirdName("third_name_4")
                .setBirthDate(LocalDate.of(2000, 2, 4))
                .setGroup("gruppa_4");

        StudentDTO studentDTO = converter.convert(studentModel, StudentDTO.class);
        StudentModel convert = converter.convert(studentDTO, StudentModel.class);

        assertEquals(studentModel, convert);
    }
}