package ru.art.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.art.dao.StudentDao;
import ru.art.dao.StudentModel;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {
    @InjectMocks
    StudentService subj;

    @Mock
    Converter converter;

    @Mock
    StudentDao studentDao;

    @BeforeEach
    void setUp() {

    }

    @Test
    void addStudent_success() {
        StudentDTO studentDTO = new StudentDTO().setId(1L);
        StudentModel studentModel = new StudentModel().setId(1L);
        when(converter.convert(studentDTO, StudentModel.class)).thenReturn(studentModel);
        when(studentDao.addStudent(studentModel)).thenReturn(studentModel);

        boolean b = subj.addStudent(studentDTO);
        assertTrue(b);

        verify(converter, times(1)).convert(studentDTO, StudentModel.class);
        verify(studentDao, times(1)).addStudent(studentModel);
    }

    @Test
    void addStudent_fail() {
        StudentDTO studentDTO = new StudentDTO().setId(1L);
        StudentModel studentModel = new StudentModel().setId(1L);
        when(converter.convert(studentDTO, StudentModel.class)).thenReturn(studentModel);
        when(studentDao.addStudent(studentModel)).thenReturn(null);

        boolean b = subj.addStudent(studentDTO);
        assertFalse(b);

        verify(converter, times(1)).convert(studentDTO, StudentModel.class);
        verify(studentDao, times(1)).addStudent(studentModel);
    }

    @Test
    void deleteStudent_success() {
        StudentDTO studentDTO = new StudentDTO().setId(1L);
        StudentModel studentModel = new StudentModel().setId(1L);
        when(converter.convert(studentDTO, StudentModel.class)).thenReturn(studentModel);
        when(studentDao.deleteStudent(studentModel)).thenReturn(true);

        boolean b = subj.deleteStudent(studentDTO);
        assertTrue(b);

        verify(converter, times(1)).convert(studentDTO, StudentModel.class);
        verify(studentDao, times(1)).deleteStudent(studentModel);
    }

    @Test
    void deleteStudent_fail() {
        StudentDTO studentDTO = new StudentDTO().setId(1L);
        StudentModel studentModel = new StudentModel().setId(1L);
        when(converter.convert(studentDTO, StudentModel.class)).thenReturn(studentModel);
        when(studentDao.deleteStudent(studentModel)).thenReturn(false);

        boolean b = subj.deleteStudent(studentDTO);
        assertFalse(b);

        verify(converter, times(1)).convert(studentDTO, StudentModel.class);
        verify(studentDao, times(1)).deleteStudent(studentModel);
    }

    @Test
    void listOfStudents() {
        StudentModel studentModel = new StudentModel().setId(1L);
        StudentDTO studentDTO = new StudentDTO().setId(1L);
        List<StudentModel> studentModels = Collections.singletonList(studentModel);
        when(converter.convert(studentModel, StudentDTO.class)).thenReturn(studentDTO);
        when(studentDao.listOfStudents()).thenReturn(studentModels);

        List<StudentDTO> studentDTOS = subj.listOfStudents();
        assertEquals(studentModel.getId(), studentDTOS.get(0).getId());

        verify(converter, times(1)).convert(studentModel, StudentDTO.class);
        verify(studentDao, times(1)).listOfStudents();

    }
}